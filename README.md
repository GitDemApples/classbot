# Classbot for LM121

This bot is designed to be as easily extendable as possible.  
It in essence you put your commands in the /src/commands folder and it will automatically pick them up

# Learning outcomes

* git
   * Become familiar with this core tool for a programmer
* Javascript/Typescript
   * Typescript is close enough to Java that you should be able to hop into it easily enough.
   * Typescript is compiled into Javascript which powers virtually every webpage
* Databases
   * This bot uses a small database called Sqlite which you need to use SQL to interact with.
* CI/CD
   * The code is tested and deployed automatically based off the ``.gitlab-ci.yml`` file.
* Code standardisation
   * This bot uses a linter called eslint to ensure that code is formatted roughly the same for everyone.

# Requirements
* [Node.js v17][node]
* [Git][git]

[node]: https://nodejs.org/en/
[git]: https://git-scm.com/downloads

# To start

1. Download the repo using git ([more info here on init and setting remote repos][intro])
2. Create yer own branch to work on, I recommend ``wip-{initials}`` for example ``wip-bg`` for me (Brendan Golden)
   * This is to ensure that only working code is on teh main branch
3. Set up the env
   1. [Follow these instructions to set up your own bot for testing][bot-setup]
   2. Copy ``.env.example`` and rename it to ``.env``
   3. Fill out the values for ``client`` and ``clientId`` in teh ``.env``
4. Run these commands:
   * ``yarn`` to install teh dependencies
   * ``yarn prepare`` to setup the linter checker
   * ``yarn build`` to compile it from typescript
   * ``yarn dev`` to start teh dev environment, this brings up your bot. If you make changes to anything in the src folder it will recompile and restart the bot
5. Make your own commands, you can use [./src/commands/assignments.ts](./src/commands/assignments.ts) as an example.
6. Run ``yarn lint`` to make sure the code is up to standard. (``yarn lint_lazy`` if you want it to be done for you)
7. Save yer changes, commit them then push your branch to the classbot repo ([see the intro doc again][intro])
8. Gitlab will then prompt you to create a merge request, to merge your ``wip-{initials}`` branch into the ``main`` branch.
   * This is the part where I (or someone I have approved) steps in and reviews yer code, if its good then it gets merged in.
   * When it gets merged in teh bot will update with your code now part of it.

I know it's a pile of text to read but if you have any questions feel free to ping me on teh discord (@Silver#5563)

[intro]: https://gitlab.com/c2842/misc/intro-to-intro-to-programming
[bot-setup]: https://discordjs.guide/preparations/setting-up-a-bot-application.html
