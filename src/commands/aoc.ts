/*
Creator:    Brendan Golden
Date:       2021-10-17
Description:This command is to manage the assignments channel
*/

import { SlashCommandBuilder, SlashCommandSubcommandBuilder } from '@discordjs/builders';
import { CommandInteraction } from "discord.js";
import { PG_Manager } from "../db";
import { AoC } from "../db_entities";
import { FindConditions } from "typeorm";
import axios from "axios";

const command_set = new SlashCommandSubcommandBuilder().setName('set').setDescription('Link Discord and AoC')
    .addNumberOption(option => option.setName('id').setDescription('AoC ID').setRequired(true))
    .addStringOption(option => option.setName('cookie').setDescription('Cookie from your browser').setRequired(false));

const command_delete = new SlashCommandSubcommandBuilder().setName('delete').setDescription('Remove yourself from the bot')
    .addNumberOption(option => option.setName('id').setDescription('AoC ID').setRequired(true))
    .addBooleanOption(option => option.setName('completely').setDescription('Remove your user from the API as well').setRequired(false));

const command_get = new SlashCommandSubcommandBuilder().setName('get').setDescription('Get your stars')
    .addNumberOption(option => option.setName('year').setDescription('Year of the AoC to get, defaults to latest').setRequired(false))
    .addBooleanOption(option => option.setName('total').setDescription('Get all years').setRequired(false))
    .addBooleanOption(option => option.setName('public').setDescription('Show publicly?').setRequired(false));

const command = new SlashCommandBuilder().setName('aoc').setDescription('Advent of Code!')
    .addSubcommand(command_set)
    .addSubcommand(command_get)
    .addSubcommand(command_delete);


/*
    Store Discord ID and AoC ID

    set:
        AoC ID (required)
        cookie (optional)

    delete:
        AoC ID (Required)

    get
        year (optional, defaults to this year is past Dec1st, last year if before)
        total (optional, sends all instead of a year)

    info?

 */


// shields.io compatible
interface API_Response {
    color: string;
    label: string;
    message: string;
    schemaVersion: number;
}

async function assignment_set(interaction: CommandInteraction, db: PG_Manager) {

    await interaction.deferReply({ ephemeral: true });


    // split out values
    const id = interaction.options.getNumber('id');
    const cookie = interaction.options.getString('cookie');

    // send request off to api
    // if no cookie check if its a valid ID
    // if cookie is present then set it then check if its a valid ID
    if (cookie) {
        // assume the user already has entered their cookie to teh api
        const result = await axios.get<string>(`https://api.brendan.ie/aoc/add?id=${id}&session=${cookie}`).catch((err) => {return { data: err.toString() };});
        if (result.data.indexOf("Successfully added user") == -1) {
            return await interaction.editReply({ content: `Error setting user. \nFor help getting cookie view https://gitlab.com/Brendan_Golden-aoc/aoc_badge_api/-/tree/main\nError: ${result.data}` });
        }
    }

    // now test if teh account exists
    try {
        const result = await axios.get<API_Response>(`https://api.brendan.ie/aoc/get/${id}/2021`);

        if (result.data.message == "User Unknown") {
            return await interaction.editReply({ content: 'User does not exist, is the id and cookie correct?' });
        }
    } catch (error) {
        return await interaction.editReply({ content: 'User does not exist, is the id and cookie correct?' });
    }


    // now we are pretty sure the account exists and works
    await db.update_one(AoC, {
        server: interaction.guildId,
        user: interaction.user.id,
        aoc_id: id,
    });


    // send a reply to the command user
    return await interaction.editReply({ content: 'User set!' });
}

async function assignment_delete(interaction: CommandInteraction, db: PG_Manager) {

    await interaction.deferReply({ ephemeral: true });

    // split out values
    const id = interaction.options.getNumber('id');
    const completely = interaction.options.getNumber('completely');


    let query: FindConditions<AoC> = {
        server: interaction.guildId,
        user: interaction.user.id,
        aoc_id: id,
    };
    if (completely) {
        try {
            const result = await axios.get<string>(`https://api.brendan.ie/aoc/delete?id=${id}`);

            if (result.data.indexOf("Successfully deleted user") == -1) {
                return await interaction.editReply({ content: 'User does not exist, are you sure it was set?' });
            }
        } catch (error) {
            return await interaction.editReply({ content: 'User does not exist, are you sure it was set?' });
        }

        // delete the user across all servers that teh user added it to
        query = {
            user: interaction.user.id,
            aoc_id: id,
        };
    }


    // only the person who added teh user can delete them
    await db.delete(AoC, query);

    // send a reply to the command user
    return await interaction.editReply({ content: `User ${id} deleted` });
}

async function assignment_get(interaction: CommandInteraction, db: PG_Manager) {

    // this command can take more than 3s to complete, so deffer it
    await interaction.deferReply({ ephemeral: !interaction.options.getBoolean('public') });

    // get the user id from the db
    const user = await db.get_items(AoC, { server: interaction.guildId, user: interaction.user.id }, ["aoc_id"]);

    if (user.length == 0) {return await interaction.editReply('No user found, please set it using ``/aoc add``');}

    const year = interaction.options.getNumber('year');
    const total = interaction.options.getBoolean('total');

    let year_query: string | number = year;
    if (!year) {
        const now = new Date();
        const year_current = now.getFullYear();

        if (now.getMonth() < 11) {
            // get last years one instead
            year_query = year_current - 1;
        } else {
            year_query = year_current;
        }
    }

    if (total) {
        year_query = "all";
    }

    // send off query
    try {
        const result = await axios.get<API_Response>(`https://api.brendan.ie/aoc/get/${user[0].aoc_id}/${year_query}`);

        if (result.data.message == "User Unknown") {
            return await interaction.editReply({ content: 'User does not exist, is the id and cookie correct?' });
        }

        const embed = {
            title: `AoC`,
            fields: [
                { name: 'User', value: `<@${interaction.user.id}>`, inline: true },
                { name: 'ID', value: `${user[0].aoc_id}`, inline: true },
                { name: 'Year', value: `${year_query}`, inline: true },
                { name: 'Stars', value: result.data.message, inline: false },
            ],
        };


        // display result
        return await interaction.editReply({ embeds:[embed] });

    } catch (error) {
        return await interaction.editReply({ content: 'User does not exist, is the id and cookie correct?' });
    }
}

module.exports = {
    data: command,

    // its named execute to standardise it across modules
    async execute(interaction: CommandInteraction, db: PG_Manager) {
        // split out by the subcommand
        switch (interaction.options.getSubcommand()) {
            case "set" : {
                return await assignment_set(interaction, db);
            }
            case "get" : {
                return await assignment_get(interaction, db);
            }
            case "delete" : {
                return await assignment_delete(interaction, db);
            }
            default: {
                await interaction.reply('Please select a subcommand');
            }
        }
    },
};
