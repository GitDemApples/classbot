import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Index } from "typeorm";

/*
These serve two functions.
They give us types in Javascript, something that it does nto natively have.
Tehy also manage our access to teh database.
*/


// ***************************************** //
// Add the entity to teh array at the bottom //
// ***************************************** //

@Entity()
// can only have one channel set per command per server
@Index(["server", "command"], { unique: true })
export class Channels {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column("text")
    server: string;

    @Column("text")
    channel: string;

    @Column("text")
    command: string;

    @Column("text")
    user: string;

    @CreateDateColumn()
    added?: Date;

    @UpdateDateColumn()
    updated?: Date;
}

@Entity()
export class Assignments {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    server: string;

    @Column("text")
    module: string;

    @Column("text")
    title: string;

    // store4d as epoch
    @Column("integer")
    due: number;

    @Column("text")
    link: string;

    @Column("text")
    user: string;

    @Column("text", { nullable: false, default: "" })
    message_id?: string;

    @CreateDateColumn()
    added?: Date;

    @UpdateDateColumn()
    updated?: Date;
}

@Entity()
export class AssignmentsReact {
	@PrimaryGeneratedColumn()
	id: number;

	@Column("text")
	user: string;

	@Column("integer")
	assignment: number;

  @Column("boolean")
  complete: boolean;

  @Column("boolean")
  notified: boolean;

}

@Entity()
export class AssignmentsUser {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("text")
  server: string;

  @Column("text")
  user: string;

  @Column("text")
  module: string;
}

@Entity()
@Index(["server", "aoc_id"], { unique: true })
export class AoC {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    server: string;

    @Column("text")
    user: string;

    @Column()
    aoc_id: number;

    @UpdateDateColumn()
    updated?: Date;
}

export const entities = [
    Channels,
    Assignments,
		AssignmentsReact,
  AssignmentsUser,
    AoC,
];